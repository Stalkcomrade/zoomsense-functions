# ZoomSense - Function

This repository contains the Firebase Cloud Functions which act as the backend for the ZoomSense project. These functions utilise external APIs, storing information in the project's [**Firebase Realtime Database**](https://firebase.google.com/docs/database) (RTDB), which is [**encrypted at rest**](https://firebase.google.com/support/privacy). It currently consists of four main modules, contained within the /functions/functions directory:

## Auth

This module creates Firebase accounts for users, using [**Zoom's OAuth API**](https://marketplace.zoom.us/docs/guides/build/oauth-app) as an intermediary. Users' basic Zoom profile information (name, email, avatar) and their authentication tokens are stored in the RTDB. This module also provides other functions related to users' Zoom profiles, such as retrieving a list of their upcoming meetings for scheduling ZoomSense integration.

### Retrieve Scheduled Zoom Meetings

When retrieving the scheduled Zoom meetings, the `page_size` query parameter defines the number of records returned within a single API call (in `auth.js`, `getZoomMeetings` function). The current implementation uses 100 as the page size for making the API call. However, if more scheduled meetings exist under the user's Zoom account, pagination can also be implemented to fulfill the need. For more information, please check [**Zoom List Meetings**](https://marketplace.zoom.us/docs/api-reference/zoom-api/meetings/meetings).

## Scheduler

Manages ZoomSense the scheduling of ZoomSense sensors. Features a [**Cloud Pub/Sub Trigger**](https://firebase.google.com/docs/functions/pubsub-events) function which is triggered every 5 minutes, and allocates Zoom Sensors to any scheduled meetings which will start within the following 30 minutes.

## Chat

Monitors and reacts to chat messages in Zoom break-out rooms, enabling interactions such as the "*bye bye sensor*" functionality.

## Google Docs

*Currently under development*

Using the Google [**Docs**](https://developers.google.com/docs/api), [**Drive**](https://developers.google.com/drive) and [**People**](https://developers.google.com/people) API scopes with the [**Node.js Google API client**](https://github.com/googleapis/google-api-nodejs-client), this module supports users in associating their Google account(s) with their existing ZoomSense account. Once authenticated, users can create Google Docs through ZoomSense, which will distribute copies of each document to the break-out rooms during the call. These documents are structured in such a way that supports the system tracking the meeting participants' progress through given activities, such as in a workshop or seminar.

## Transcoder

Sets a queue item to process any new meeting recording files that enter the system.

# ZoomSense - Rules

This repository also contains Firebase Realtime Database rules and Storage rules for secured data access:

- **Realtime Database Rules**: `database.json`
- **Storage Rules**: `storage.rules`

# Building, Testing, and Deploying

Running and deploying this project requires the installation of the [**Firebase CLI**](https://firebase.google.com/docs/cli). If using Windows, Git Bash doesn't work - Windows Powershell seems to work best.

If you're using Windows Powershell, you will probably have to run the following command each session you want to use the Firebase CLI:

```
Set-ExecutionPolicy -Scope Process -ExecutionPolicy Bypass
```

To support a dedicated live development environment, ZoomSense functions has two separate Firebase projects: "zoombot" (production) and "zoomsensedev" (development). These projects connect to different API instances and databases. Once you are logged into an authenticated Google account, they can be switched between using the Firebase CLI command:

```
firebase use project_name
```

The sample environment variables for the Firebase Functions have been provided in the `/functions/.runtimeconfig.json` file. More details for those credentials have been listed below:

```JSON
"project_name": {
    "firebase_rtdb": "https://*******.firebaseio.com/",
    "site_url": "https://*******.web.app",
    "functions_url": "https://us-central1-*******.cloudfunctions.net/",
    "localhost_functions_url": "http://localhost:5000/*******/us-central1/",
    "domain_verification": "Used for the Google Docs webhook, it can be obtained from https://support.google.com/webmasters/answer/9008080?hl=en by choosing `VERIFY YOUR PROPERTY` and `Add Property` for the web client’s site URL",
    "zoom": {
      "client_id": "Zoom OAuth App Client ID",
      "client_secret": "Zoom OAuth App Client Secret"
    },
    "firebase_credentials": {
      "type": "service_account",
      "project_id": "*******",
      "private_key_id": "*******",
      "private_key": "-----BEGIN PRIVATE KEY-----********\n-----END PRIVATE KEY-----\n",
      "client_email": "*******@appspot.gserviceaccount.com",
      "client_id": "*******",
      "auth_uri": "https://accounts.google.com/o/oauth2/auth",
      "token_uri": "https://oauth2.googleapis.com/token",
      "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
      "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/*******%40appspot.gserviceaccount.com"
    },
    "gapi": {
      "client_id": "Google OAuth 2.0 Client ID",
      "client_secret": "Google OAuth 2.0 Client Secret",
      "api_key": "Can be found on https://console.developers.google.com by navigating to Credentials > API Keys > Browser Key"
    },
    "anonymous": {
      "encryption_key": "Encryption Key String",
      "encryption_secret": "Encryption Secret String"
    }
  },
```

Running in the offline emulator will require you to populate the relevant sections of the included .runtimeconfig.json file with real data. The environment variables should already be set for the live versions. These can be found and stored for local use, using the command:

```
(Mac/Linux) firebase functions:config:get > .runtimeconfig.json 
(Windows Powershell) firebase functions:config:get | ac .runtimeconfig.json
```

Once the .runtimeconfig.json file is prepared and in the /functions folder, Functions can be tested locally using the command:

```
firebase emulators:start --only functions
```

If you need to add new variables, the .runtimeconfig.json file should be updated to reflect the structure. You add several new variables at once, using the command structure:

```
firebase functions:config:set project_name.api_name.key_name=value zoomsensedev.zoom.client_secret="****" zoomsensedev.firebase_credentials.private_key="****"
```

You can then deploy any updated Firebase Functions, config variables and Rules to the live version of that environment with: 

```
(All changes) firebase deploy
(Only Functions and config changes) firebase deploy  --only functions
(Config changes and specific Functions) firebase deploy --only functions:function1,functions:function2
```