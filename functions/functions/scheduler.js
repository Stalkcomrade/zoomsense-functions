const functions = require("firebase-functions");
const admin = require("firebase-admin");
const { getMinDiff } = require("../utils/timeformatter");

const db = admin.database();
const meetingRef = db.ref("meetings");
const whitelistRef = db.ref("whitelist");
const schedulingRef = db.ref("scheduling");

// Firebase Cloud Function scheduled at a 5 minutes interval
exports.zoomScheduler = functions.pubsub
  .schedule("every 1 minutes")
  .timeZone("Australia/Melbourne")
  .onRun(async (context) => {
    try {
      // Retrieve whitelisted Uids
      const whitelistSnap = await whitelistRef.once("value");
      const whitelist = await getWhitelistIds(whitelistSnap.val());

      // Retrieve all meetings need to be scheduled
      const meetingSnap = await meetingRef.once("value");
      const meetings = meetingSnap.val();
      const scheduleMeetings = await getScheduleMeetings(meetings, whitelist);

      // Perform scheduling for meetings which will start within 30 minutes
      const schedulingSnap = await schedulingRef.once("value");
      const scheduling = schedulingSnap.val();
      await pushSchedules(scheduling, scheduleMeetings);
    } catch (error) {
      console.error(error);
    }
  });

/**
 * Get all whitelisted uids from Firebase
 * @param {*} whitelist Whitelist snapshot in Firebase
 */
async function getWhitelistIds(whitelist) {
  const allUsers = await admin.auth().listUsers();

  const userList = [];
  for (let i = 0; i < allUsers.users.length; i++) {
    const user = allUsers.users[i];
    userList[user.email] = user.uid;
  }

  const whitelistUids = [];
  const userListKeys = Object.keys(userList);
  userListKeys.forEach((userKey) => {
    if (whitelist.includes(userKey)) whitelistUids.push(userList[userKey]);
  });

  return whitelistUids;
}

/**
 * Get meetings which need to be scheduled
 * @param {*} meetings Meeting snapshot in Firebase
 * @param {*} whitelist Whitelist snapshot in Firebase
 */
async function getScheduleMeetings(meetings, whitelist) {
  const uidKeys = Object.keys(meetings);
  const promises = [];
  const scheduleMeetings = [];

  // Loop through all uids under the meeting node
  uidKeys.forEach(async (uid) => {
    const uidMeetings = meetings[uid];
    const meetingKeys = Object.keys(uidMeetings);

    // If the uid is not in the whitelist, skip scheduling
    if (!whitelist.includes(uid)) {
      meetingKeys.forEach((meetingKey) => {
        promises.push(
          meetingRef.child(`${uid}/${meetingKey}`).update({ scheduled: false })
        );
      });
    }
    // Otherwise, calculate the time differences between the current time and schedule time
    // If the meeting will start within 30 minutes, then add to the scheduling meeting list
    else {
      meetingKeys.forEach((meetingKey) => {
        let meeting = uidMeetings[meetingKey];
        meeting.meetingNo = meetingKey;
        meeting.uid = uid;
        const scheduled = meeting.scheduled;
        let startTime = meeting.startTime;

        if (startTime) {
          const diffMins = getMinDiff(startTime);
          if (scheduled === undefined && diffMins <= 30) {
            console.log(`Start Zoom Sense for ${meetingKey} at ${startTime}`);
            promises.push(
              meetingRef
                .child(`${uid}/${meetingKey}`)
                .update({ scheduled: true })
            );
            scheduleMeetings.push(meeting);
          }
        } else {
          promises.push(
            meetingRef
              .child(`${uid}/${meetingKey}`)
              .update({ error: "Missing start time of the meeting" })
          );
        }
      });
    }
  });

  await Promise.all(promises);
  return scheduleMeetings;
}

/**
 * Push schedulings to Firebase based on the resource availability
 * @param {*} scheduling Scheduling snapshot in Firebase
 * @param {*} scheduleMeetings Meeting list which need to be scheduled
 */
async function pushSchedules(scheduling, scheduleMeetings) {
  let tempScheduling = scheduling;
  const keys = Object.keys(tempScheduling);
  const promises = [];

  // Loop through all the meetings need to be scheduled
  for (let i = 0; i < scheduleMeetings.length; i++) {
    const meetingToSchedule = scheduleMeetings[i];
    const noBo = meetingToSchedule.noOfBreakoutRooms;
    let allocateIndex = 0;

    // Check all vms currently running
    for (let j = 0; j < keys.length; j++) {
      const vm = tempScheduling[keys[j]];
      const capacity = vm.capacity;
      const sensors = vm.sensors;

      // Get the number of sensors currently running
      let inUse = !sensors ? 0 : Object.keys(sensors).length;
      if (inUse === 0) tempScheduling[keys[j]].sensors = [];

      // Allocate sensors if the vm still has capacity
      const startIndex = allocateIndex;
      for (let k = startIndex; k < noBo; k++) {
        if (inUse < capacity) {
          const sensorIndex = `${meetingToSchedule.meetingNo}-${k + 1}`;
          const key = `${keys[j]}/sensors/${sensorIndex}`;
          const value = {
            startTime: meetingToSchedule.startTime,
            uid: meetingToSchedule.uid,
            password: meetingToSchedule.password,
          };
          promises.push(schedulingRef.child(key).set(value));

          // Record VM Allocation under the meeting node
          promises.push(
            meetingRef
              .child(
                `${meetingToSchedule.uid}/${meetingToSchedule.meetingNo
                }/vmAllocation/ZoomSensor_${k + 1}`
              )
              .set(keys[j])
          );
          inUse++;
          allocateIndex++;
          tempScheduling[keys[j]].sensors[key] = value;
        } else break;
      }

      // Stop allocation for the current meeting if all bos have been assigned
      if (allocateIndex === noBo) break;
    }

    // If not enough resources available, set unallocated number to the meeting node
    if (allocateIndex < noBo) {
      const unallocated = noBo - allocateIndex;
      promises.push(
        meetingRef
          .child(
            `${meetingToSchedule.uid}/${meetingToSchedule.meetingNo}/unallocated`
          )
          .set(unallocated)
      );
    }
  }

  await Promise.all(promises);
}
