const functions = require("firebase-functions");
const admin = require("firebase-admin");

const db = admin.database();
const meetingRef = db.ref("meetings");
const processingRef = db.ref("processing").child("recordings");

// Sets a queue item to process any new meeting recording files that enter the system.
exports.updateProcessingQueue = functions.storage
  .object()
  .onFinalize(async (object) => {
    if (object.name.endsWith("_02.meetingrec")) {
      const queueitem = {
        fileBucket: object.bucket,
        filePath: object.name,
        created: object.timeCreated,
      };

      const filePathArr = object.name.split("/");
      const uid = filePathArr[0];
      const meetingId = filePathArr[1];
      const sensorId = filePathArr[2].split("-")[0];

      const snapshot = await meetingRef
        .child(`${uid}/${meetingId}/vmAllocation/${sensorId}`)
        .once("value");
      const vm = snapshot.val();
      await processingRef.child(vm).push(queueitem);
    }
  });
