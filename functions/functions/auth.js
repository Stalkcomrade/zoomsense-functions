const functions = require("firebase-functions");
const admin = require("firebase-admin");
const cookieParser = require("cookie-parser");
const crypto = require("crypto");
const simpleOAuth = require("simple-oauth2");
const request = require("request");
const cors = require("cors")({
  origin: "*",
});

const localHost = false;
const environment = functions.config()[process.env.GCLOUD_PROJECT];
const db = admin.database();
const OAUTH_SCOPES = "user:read";
const OAUTH_REDIRECT_URI =
  (localHost
    ? environment.localhost_functions_url
    : environment.functions_url) + `authtokenredirect`;

// https://marketplace.zoom.us/docs/guides/auth/oauth

// Login flow
// 1) Get Auth code from Zoom (requires user auth)
// 2) Auth code gets returns to OAUTH_REDIRECT_URI
// 3) Use code to get access token (lasts 1 hour)
// 4) Access token gets returned to ACCESS_REDIRECT_URI
// 4) Use access token to request user and meeting details from Zoom

// Existing user flow
// 1) Check access token is valid (if yes, go to 3)
// 2) Refresh access token using auth code
// 3) Use access token to request user and meeting details from Zoom

function zoomOAuth2Client() {
  // Zoom OAuth 2 setup
  const credentials = {
    client: {
      id: environment.zoom.client_id,
      secret: environment.zoom.client_secret,
    },
    auth: {
      tokenHost: "https://zoom.us",
      tokenPath: "/oauth/authorize",
    },
  };
  return simpleOAuth.create(credentials);
}

/**
 * Redirects the User to the Zoom authentication consent screen. Also the 'state' cookie is set for later state
 * verification. Requires a host be passed through the returnURL param, which will eventually be sent the firebase ID token
 */
exports.requestZoomAuth = functions.https.onRequest((req, res) => {
  const oauth2 = zoomOAuth2Client();
  var returnUrl = req.query.return;

  if (!returnUrl) {
    console.log("No return url given, using default");
    returnUrl = environment.site_url;
  }

  cookieParser()(req, res, () => {
    const state = req.cookies.state || crypto.randomBytes(20).toString("hex");
    console.log("Setting verification state:", state);
    res.cookie("state", state.toString(), {
      maxAge: 3600000,
      secure: true,
      httpOnly: true,
    });
    const redirectUri = oauth2.authorizationCode.authorizeURL({
      redirect_uri: OAUTH_REDIRECT_URI,
      scope: OAUTH_SCOPES,
      state: state + "!RETURN!" + returnUrl,
    });
    console.log("Redirecting to:", redirectUri);
    res.redirect(redirectUri);
  });
});

/**
 * Exchanges a given Zoom auth code passed in the 'code' URL query parameter for a Firebase auth token.
 * The request also needs to specify a 'state' query parameter which will be checked against the 'state' cookie.
 */
exports.authtokenredirect = functions.https.onRequest((req, res) => {
  try {
    cookieParser()(req, res, () => {
      console.log("Received verification state:", req.cookies.state);

      var stateData = req.query.state.split("!RETURN!");
      var returnUrl =
        stateData.length < 2 ? environment.site_url : stateData[1];

      console.log("using return url " + returnUrl);

      console.log("Received state:", stateData[0]);
      if (!localHost && !req.cookies.state) {
        throw new Error(
          "State cookie not set or expired. Maybe you took too long to authorize. Please try again."
        );
      } else if (!localHost && req.cookies.state !== stateData[0]) {
        throw new Error("State validation failed");
      }

      var zoom_tokens = {
        auth: req.query.code,
      };

      getAccessTokenFromAuthCode(req.query.code)
        .then((tokens) => {
          zoom_tokens.refresh = tokens.refresh_token;
          zoom_tokens.access = tokens.access_token;
          zoom_tokens.access_expires = getExpireDate(tokens.expires_in);

          return getZoomUserProfile(tokens.access_token);
        })
        .then((userDetails) => {
          var googAcc = {
            uid: `zoom:${userDetails.id}`,
            email: userDetails.email,
            displayName: userDetails.first_name + " " + userDetails.last_name,
            photoURL: userDetails.pic_url,
          };

          // Create a Firebase account and get the Custom Auth Token.
          return createFirebaseAccount(googAcc, zoom_tokens);
        })
        .then((firebaseIdToken) => {
          return res.redirect(returnUrl + "#/login?token=" + firebaseIdToken);
        })
        .catch((err) => {
          console.error("signUpChainErr", err);
          return res.status(500).send({ err: err });
        });
    });
  } catch (error) {
    console.error("authtokenredirectErr", error);
    return res.jsonp({ error: error.toString });
  }
});

exports.getProfile = functions.https.onRequest((req, res) => {
  return cors(req, res, () => {
    const idToken = req.body.idToken.toString();
    console.log("Received token", idToken);

    admin
      .auth()
      .verifyIdToken(idToken)
      .then((decodedClaims) => {
        console.log("decoded claims", decodedClaims);
        return admin.auth().getUser(decodedClaims.uid);
      })
      .then((userRecord) => {
        // See the UserRecord reference doc for the contents of userRecord.
        console.log("Successfully fetched user data: ", userRecord.toJSON());
        return res.jsonp(userRecord.toJSON());
      })
      .catch((error) => {
        console.error("token err", error);
        return res.status(401).send("Session token is unavailable or invalid");
      });
  });
});

exports.getScheduledMeetings = functions.https.onRequest((req, res) => {
  return cors(req, res, () => {
    const idToken = req.body.idToken.toString();

    // Verify ZoomSense user
    admin
      .auth()
      .verifyIdToken(idToken)
      // Get a valid zoom access token for this user
      .then((decodedClaims) => {
        return getValidZoomAccessToken(decodedClaims.uid);
      })
      // Use the access token to request their scheduled meetings
      .then((accessToken) => {
        return getZoomMeetings(accessToken, false);
      })
      // Return the meetings
      .then((meetings) => {
        return res.jsonp(meetings);
      })
      .catch((error) => {
        console.error("token err", error);
        return res.status(401).send("Session token is unavailable or invalid");
      });
  });
});

exports.getMeetingPassword = functions.https.onRequest((req, res) => {
  return cors(req, res, () => {
    if (!req.body.idToken || !req.body.meetingid) return res.sendStatus(400);

    const idToken = req.body.idToken.toString();
    const meetingid = req.body.meetingid.toString();
    console.log("Received token", idToken);

    // Verify ZoomSense user
    admin
      .auth()
      .verifyIdToken(idToken)
      // Get a valid zoom access token for this user
      .then((decodedClaims) => {
        return getValidZoomAccessToken(decodedClaims.uid);
      })
      // Use the access token to request their scheduled meetings
      .then((accessToken) => {
        return getMeetingDetails(accessToken, meetingid);
      })
      // Return the meeting details
      .then(async (meeting) => {
        console.log("Successfully fetched user meeting: ", meeting);
        return res.jsonp(meeting.password);
      })
      .catch((error) => {
        console.error("token err", error);
        return res.status(401).send("Session token is unavailable or invalid");
      });
  });
});

function getAccessTokenFromAuthCode(authCode) {
  return new Promise((resolve, reject) => {
    let url =
      "https://zoom.us/oauth/token?grant_type=authorization_code&code=" +
      authCode +
      "&redirect_uri=" +
      OAUTH_REDIRECT_URI;

    request
      .post(url, (error, response, body) => {
        if (error) {
          console.error("zoom access token error", error);
          reject(error);
        }

        // Parse response to JSON
        var toRet = JSON.parse(body);

        if (toRet.access_token) {
          resolve(toRet);
        } else {
          reject(new Error("No access token returned"));
        }
      })
      .auth(environment.zoom.client_id, environment.zoom.client_secret);
  });
}

function getValidZoomAccessToken(uid) {
  return admin
    .database()
    .ref(`/zoomtokens/${uid}`)
    .once("value")
    .then((tokens) => {
      if (!tokens) {
        reject(new Error("No tokens found for user id: ", uid));
      }

      var tokensVal = tokens.val();
      
      // If we have a comfortably valid (at least 6 seconds left) access token already, return that
      if (
        tokensVal.access &&
        new Date(tokensVal.access_expires - 6000) > Date.now()
      ) {
        return tokensVal.access;
      }

      // Otherwise, refresh access token from zoom
      let url =
        "https://zoom.us/oauth/token?grant_type=refresh_token&refresh_token=" +
        tokensVal.refresh;

      return new Promise((resolve, reject) => {
        request
          .post(url, (error, response, body) => {
            if (error || body.error) {
              console.error("zoom refresh token error", error);
              reject(error);
            }

            console.log("resp", body);

            // Parse response to JSON
            var newTokens = JSON.parse(body);

            if (!newTokens) {
              return reject(new Error("No access token returned"));
            }

            tokensVal.access = newTokens.access_token;
            tokensVal.refresh = newTokens.refresh_token;
            tokensVal.access_expires = getExpireDate(newTokens.expires_in);

            // Store updated token details
            db.ref(`/zoomtokens/${uid}`).set(tokensVal);

            return resolve(tokensVal.access);
          })
          .auth(environment.zoom.client_id, environment.zoom.client_secret);
      });
    });
}

function getZoomUserProfile(accessToken) {
  return new Promise((resolve, reject) => {
    request
      .get("https://api.zoom.us/v2/users/me", (error, response, body) => {
        if (error) {
          console.error("Zoom API Response Error: ", error);
          reject(error);
        } else {
          body = JSON.parse(body);
          resolve(body);
        }
      })
      .auth(null, null, true, accessToken);
  });
}

function getZoomMeetings(accessToken, includePast = false) {
  var reqType = includePast ? "scheduled" : "upcoming";

  return new Promise((resolve, reject) => {
    request
      .get(
        `https://api.zoom.us/v2/users/me/meetings?type=${reqType}&page_size=100`,
        (error, response, body) => {
          if (error) {
            console.error("Zoom API Response Error: ", error);
            reject(error);
          } else {
            resolve(JSON.parse(body));
          }
        }
      )
      .auth(null, null, true, accessToken);
  });
}

function getMeetingDetails(accessToken, meetingid) {
  return new Promise((resolve, reject) => {
    request
      .get(
        `https://api.zoom.us/v2/meetings/${meetingid}`,
        (error, response, body) => {
          if (error) {
            console.error("Zoom API Response Error: ", error);
            reject(error);
          } else {
            resolve(JSON.parse(body));
          }
        }
      )
      .auth(null, null, true, accessToken);
  });
}

function getExpireDate(expires_in) {
  return new Date().getTime() + expires_in * 1000;
}

/**
 * Creates a Firebase account with the given user profile and returns a custom auth token allowing
 * signing-in this account.
 * Also saves the accessToken to the datastore at /zoomAccessToken/$uid
 *
 * @returns {Promise<string>} The Firebase custom auth token in a promise.
 */
function createFirebaseAccount(googAcc, zoom_tokens) {
  // Save the access token to the Firebase Realtime Database.
  const databaseTask = admin
    .database()
    .ref(`/zoomtokens/${googAcc.uid}`)
    .set(zoom_tokens);
  // Create or update the user account.
  const userCreationTask = admin
    .auth()
    .updateUser(googAcc.uid, googAcc)
    .catch((error) => {
      // If user does not exists we create it.
      if (error.code === "auth/user-not-found") {
        return admin.auth().createUser(googAcc);
      }
      throw error;
    });
  // Wait for all async task to complete then generate and return a custom auth token.
  return Promise.all([userCreationTask, databaseTask]).then(() => {
    // Create a Firebase custom auth token.
    return admin
      .auth()
      .createCustomToken(googAcc.uid)
      .then((token) => {
        console.log(
          'Created Custom token for UID "',
          googAcc.uid,
          '" Token:',
          token
        );
        return token;
      });
  });
}
