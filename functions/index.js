const functions = require("firebase-functions");
const admin = require("firebase-admin");
const environment = functions.config()[process.env.GCLOUD_PROJECT];

admin.initializeApp({
  credential: admin.credential.cert({
    projectId: environment.firebase_credentials.project_id,
    clientEmail: environment.firebase_credentials.client_email,
    privateKey: environment.firebase_credentials.private_key.replace(
      /\\n/g,
      "\n"
    ), // remove any added escape slashes
  }),
  databaseURL: environment.firebase_rtdb,
});

const auth = require("./functions/auth");
const chat = require("./functions/chat");
const scheduler = require("./functions/scheduler");
const transcoder = require("./functions/transcoder");
const prompt = require("./functions/prompt");
const gdocs = require("./functions/gdocs");
const anonymous = require("./functions/anonymous");

exports.requestZoomAuth = auth.requestZoomAuth;
exports.authtokenredirect = auth.authtokenredirect;
exports.getProfile = auth.getProfile;
exports.getScheduledMeetings = auth.getScheduledMeetings;
exports.getMeetingPassword = auth.getMeetingPassword;
exports.checkChatMessage = chat.checkChatMessage;
exports.decryptToken = anonymous.decryptToken;
exports.generateLink = anonymous.generateLink;
exports.zoomScheduler = scheduler.zoomScheduler;
exports.updateProcessingQueue = transcoder.updateProcessingQueue;
exports.promptScheduler = prompt.promptScheduler;
// exports.promptSchedulerLA = prompt.promptSchedulerLA;
exports.googleDocsAuth = gdocs.googleDocsAuth;
exports.googleDocsRedirect = gdocs.googleDocsRedirect;
exports.googleDocsAddOrUpdateDocument = gdocs.googleDocsAddOrUpdateDocument;
exports.googleDocsDeleteDocument = gdocs.googleDocsDeleteDocument;
exports.googleDocsWatchingWebHook = gdocs.googleDocsWatchingWebHook;
exports.googleDocsDistributeAndStartWatchingCopies = gdocs.googleDocsDistributeAndStartWatchingCopies;