const moment = require("moment-timezone");

/**
 * Calculate the minutes difference between the scheduled and current date time
 * @param {*} scheduledDate Meeting scheduled date time
 */
function getMinDiff(startTime) {
  let current = moment().utc().toDate();
  const diffMs = moment(startTime).toDate() - current;

  return Math.floor(diffMs / 1000 / 60);
}

exports.getMinDiff = getMinDiff;
